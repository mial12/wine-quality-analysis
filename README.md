Data analysis project predicting wine quality based on chemical composition using logistic regression and decision trees. Includes descriptive statistics, classification and statistical tests.
Based on data from Kaggle (https://www.kaggle.com/datasets/yasserh/wine-quality-dataset).
